# eslint-config-sncf

An eslint-config made for TypeScript.

## Installation

```
npm i --save-dev @sncf/eslint-config
```

## Usage

Once the `@sncf/eslint-config` package is installed, you can use it by specifying `"@sncf/eslint-config"` in the [`extends`](http://eslint.org/docs/user-guide/configuring#extending-configuration-files) section of your [ESLint configuration](http://eslint.org/docs/user-guide/configuring).

```js
{
  "extends": "@sncf/eslint-config",
  "rules": {
    // Additional, per-project rules...
  }
}
```
