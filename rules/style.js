/*
 This file may be delete in the future in favor of a formatter tool
 see https://typescript-eslint.io/linting/troubleshooting/formatting/
*/
module.exports = {
	rules: {
		// Enforce consistent spacing after the // or /* in a comment
		// https://eslint.org/docs/latest/rules/spaced-comment
		'spaced-comment': ['warn', 'always'],

		// Enforce consistent spacing before and after the arrow in arrow functions
		// https://eslint.org/docs/latest/rules/arrow-spacing
		'arrow-spacing': 'warn',

		// Disallow or enforce spaces inside of blocks after opening block and before closing block
		// https://eslint.org/docs/latest/rules/block-spacing
		'block-spacing': 'off',
		// https://typescript-eslint.io/rules/block-spacing
		'@typescript-eslint/block-spacing': ['warn', 'always'],

		// Enforce consistent brace style for blocks
		// https://eslint.org/docs/latest/rules/brace-style
		'brace-style': 'off',
		// https://typescript-eslint.io/rules/brace-style
		'@typescript-eslint/brace-style': ['warn', '1tbs'],

		// Enforce consistent spacing before and after commas
		// https://eslint.org/docs/latest/rules/comma-spacing
		'comma-spacing': 'off',
		// https://typescript-eslint.io/rules/comma-spacing
		'@typescript-eslint/comma-spacing': ['warn', {'before': false, 'after': true}],

		// Enforce consistent comma style
		// https://eslint.org/docs/latest/rules/comma-style
		'comma-style': ['warn', 'last'],

		// Enforce consistent spacing inside computed property brackets
		// https://eslint.org/docs/latest/rules/computed-property-spacing
		'computed-property-spacing': ['warn', 'never'],

		// Require or disallow newline at the end of files
		// https://eslint.org/docs/latest/rules/eol-last
		'eol-last': ['warn', 'always'],

		// Require or disallow spacing between function identifiers and their invocations
		// https://eslint.org/docs/latest/rules/func-call-spacing
		'func-call-spacing': 'off',
		// https://typescript-eslint.io/rules/func-call-spacing
		'@typescript-eslint/func-call-spacing': ['warn', 'never'],

		// TODO this rule is deactivated regarding this bug https://github.com/typescript-eslint/typescript-eslint/issues/1824
		// Enforce consistent indentation
		// https://eslint.org/docs/latest/rules/indent
		// 'indent': ['warn', 2, {'SwitchCase': 1}],

		// Enforce the consistent use of either double or single quotes in JSX attributes
		// https://eslint.org/docs/latest/rules/jsx-quotes
		'jsx-quotes': ['warn', 'prefer-single'],

		// Enforce consistent spacing between keys and values in object literal properties
		// https://eslint.org/docs/latest/rules/key-spacing
		'key-spacing': 'off',
		// https://typescript-eslint.io/rules/key-spacing
		'@typescript-eslint/key-spacing': ['warn', {'beforeColon': false, 'afterColon': true, 'mode': 'strict'}],

		// Enforce a maximum line length
		// https://eslint.org/docs/latest/rules/max-len
		'max-len': ['warn', {'code': 200}],

		// Disallow mixed spaces and tabs for indentation
		// https://eslint.org/docs/latest/rules/no-mixed-spaces-and-tabs
		'no-mixed-spaces-and-tabs': 'warn',

		// Disallow multiple spaces
		// https://eslint.org/docs/latest/rules/no-multi-spaces
		'no-multi-spaces': 'warn',

		// Disallow multiple empty lines
		// https://eslint.org/docs/latest/rules/no-multiple-empty-lines
		'no-multiple-empty-lines': ['warn', {'max': 1, 'maxEOF': 1}],

		// Disallow trailing whitespace at the end of lines
		// https://eslint.org/docs/latest/rules/no-trailing-spaces
		'no-trailing-spaces': 'warn',

		// Disallow whitespace before properties
		// https://eslint.org/docs/latest/rules/no-whitespace-before-property
		'no-whitespace-before-property': 'warn',

		// Enforce consistent spacing inside braces
		// https://eslint.org/docs/latest/rules/object-curly-spacing
		'object-curly-spacing': 'off',
		// https://typescript-eslint.io/rules/object-curly-spacing
		'@typescript-eslint/object-curly-spacing': ['warn', 'always'],

		// Require or disallow padding within blocks
		// https://eslint.org/docs/latest/rules/padded-blocks
		'padded-blocks': ['warn', 'never'],

		// Enforce the consistent use of either  double, or single quotes
		// https://eslint.org/docs/latest/rules/quotes
		'quotes': 'off',
		// https://typescript-eslint.io/rules/quotes
		'@typescript-eslint/quotes': ['warn', 'single'],

		// Require or disallow semicolons instead of ASI
		// https://eslint.org/docs/latest/rules/semi
		'semi': 'off',
		// https://typescript-eslint.io/rules/semi
		'@typescript-eslint/semi': ['warn', 'always'],

		// Enforce location of semicolons
		// https://eslint.org/docs/latest/rules/semi-style
		'semi-style': ['warn', 'last'],

		// Enforce consistent spacing before blocks
		// https://eslint.org/docs/latest/rules/spae-before-blocks
		'space-before-blocks': 'off',
		// https://typescript-eslint.io/rules/space-before-blocks
		'@typescript-eslint/space-before-blocks': ['warn', 'always'],

		// Enforce consistent spacing before function definition opening parenthesis
		// https://eslint.org/docs/latest/rules/space-before-function-paren
		'space-before-function-paren': 'off',
		// https://typescript-eslint.io/rules/space-before-function-paren
		'@typescript-eslint/space-before-function-paren': ['warn', 'never'],

		// Require spacing around infix operators
		// https://eslint.org/docs/latest/rules/spaced-infix-ops
		'space-infix-ops': 'off',
		// https://typescript-eslint.io/rules/space-infix-ops
		'@typescript-eslint/space-infix-ops': 'warn',

		// Enforce consistent spacing before or after unary operators
		// https://eslint.org/docs/latest/rules/space-unary-ops
		'space-unary-ops': ['warn', {'words': true, 'nonwords': false}],

		// Enforce spacing around colons of switch statements
		// https://eslint.org/docs/latest/rules/switch-colon-spacing
		'switch-colon-spacing': ['warn', {'after': true, 'before': false}]
	}
};
