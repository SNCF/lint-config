module.exports = {
    rules: {
        // Enforce 'for' loop update clause moving the counter in the right direction
        // https://eslint.org/docs/latest/rules/for-direction
        'for-direction': 'error',

        // Enforce return statements in getters
        // https://eslint.org/docs/latest/rules/getter-return
        'getter-return': 'error',

        // Disallow reassigning class members
        // https://eslint.org/docs/latest/rules/no-class-assign
        'no-class-assign': 'error',

        // Disallow assignment operators in conditional expressions
        // https://eslint.org/docs/latest/rules/no-cond-assign
        'no-cond-assign': 'error',

        // Disallow reassigning const variables
        // https://eslint.org/docs/latest/rules/no-const-assign
        'no-const-assign': 'error',

        // Disallow duplicate class members
        // https://eslint.org/docs/latest/rules/no-dupe-class-members
        'no-dupe-class-members': 'off',
        // https://typescript-eslint.io/rules/no-dupe-class-members
        '@typescript-eslint/no-dupe-class-members': 'error',

        // Disallow duplicate keys in object literals
        // https://eslint.org/docs/latest/rules/no-dupe-keys
        'no-dupe-keys': 'error',

        // Disallow duplicate case labels
        // https://eslint.org/docs/latest/rules/no-duplicate-case
        'no-duplicate-case': 'error',

        // Disallow duplicate module imports
        // https://eslint.org/docs/latest/rules/no-duplicate-imports
        'no-duplicate-imports': 'off',
        // https://typescript-eslint.io/rules/no-duplicate-imports
        '@typescript-eslint/no-duplicate-imports': 'error',

        // disallow adding to native types
        // https://eslint.org/docs/rules/no-extend-native
        'no-extend-native': 'error',

        // Disallow reassigning function declarations
        // https://eslint.org/docs/latest/rules/no-func-assign
        'no-func-assign': 'error',

        // disallow reassignments of native objects or read-only globals
        // https://eslint.org/docs/rules/no-global-assign
        'no-global-assign': ['error', {'exceptions': []}],

        // Disallow assigning to imported bindings
        // https://eslint.org/docs/latest/rules/no-import-assign
        'no-import-assign': 'error',

        // Disallow variable or function declarations in nested blocks
        // https://eslint.org/docs/latest/rules/no-inner-declarations
        'no-inner-declarations': 'error',

        // Disallow irregular whitespace
        // https://eslint.org/docs/latest/rules/no-irregular-whitespace
        'no-irregular-whitespace': 'error',

        // disallow creation of functions within loops
        // https://eslint.org/docs/rules/no-loop-func
        'no-loop-func': 'off',
        // https://typescript-eslint.io/rules/no-loop-func
        '@typescript-eslint/no-loop-func': 'error',

        // Disallow characters which are made with multiple code points in character class syntax
        // https://eslint.org/docs/latest/rules/no-misleading-character-class
        'no-misleading-character-class': 'error',

        // Disallow calling global object properties as functions
        // https://eslint.org/docs/latest/rules/no-obj-calls
        'no-obj-calls': 'error',

        // Disallow assignments where both sides are exactly the same
        // https://eslint.org/docs/latest/rules/no-self-assign
        'no-self-assign': 'error',

        // disallow comparisons where both sides are exactly the same
        // https://eslint.org/docs/rules/no-self-compare
        'no-self-compare': 'error',

        // Disallow returning values from setters
        // https://eslint.org/docs/latest/rules/no-this-before-super
        'no-this-before-super': 'error',

        // Disallow this/super before calling super() in constructors
        // https://eslint.org/docs/latest/rules/no-undef
        'no-undef': 'error',

        // Disallow confusing multiline expressions
        // https://eslint.org/docs/latest/rules/no-unexpected-multiline
        'no-unexpected-multiline': 'error',

        // Disallow negating the left operand of relational operators
        // https://eslint.org/docs/latest/rules/no-unsafe-negation
        'no-unsafe-negation': 'error',

        // disallow use of the with statement
        // https://eslint.org/docs/rules/no-with
        'no-with': 'error',

        // Enforce comparing typeof expressions against valid strings
        // https://eslint.org/docs/latest/rules/valid-typeof
        'valid-typeof': 'error',

        // https://typescript-eslint.io/rules/no-inferrable-types
        '@typescript-eslint/no-inferrable-types': 'off',

        // https://typescript-eslint.io/rules/explicit-module-boundary-types
        '@typescript-eslint/explicit-module-boundary-types': 'off',

        // https://typescript-eslint.io/rules/ban-ts-comment
        '@typescript-eslint/ban-ts-comment': 'off',
    }
};
