module.exports = {
    rules: {
        // enforce that class methods use 'this'
        // https://eslint.org/docs/rules/class-methods-use-this
        'class-methods-use-this': ['warn', {
            'exceptMethods': []
        }],

        // specify curly brace conventions for all control statements
        // https://eslint.org/docs/rules/curly
        'curly': ['warn', 'multi-line'], // multiline

        // require the use of === and !==
        // https://eslint.org/docs/rules/eqeqeq
        'eqeqeq': ['warn', 'always', {'null': 'ignore'}],

        // This rule disallows empty block statements. This rule ignores block statements which contain a comment
        // https://eslint.org/docs/latest/rules/no-empty
        'no-empty': 'warn',

        // Disallow unnecessary boolean casts
        // https://eslint.org/docs/latest/rules/no-extra-boolean-cast
        'no-extra-boolean-cast': 'warn',

        // disallow this keywords outside of classes or class-like objects
        // https://eslint.org/docs/rules/no-invalid-this
        'no-invalid-this': 'off',
        '@typescript-eslint/no-invalid-this': 'warn',

        // Disallow multiple spaces in regular expressions
        // https://eslint.org/docs/latest/rules/no-regex-spaces
        'no-regex-spaces': 'warn',

        // Disallow identifiers from shadowing restricted names (Nan, Infinity…)
        // https://eslint.org/docs/latest/rules/no-shadow-restricted-names
        'no-shadow-restricted-names': 'warn',

        // Disallow unused labels
        // https://eslint.org/docs/latest/rules/no-unused-labels
        'no-unused-labels': 'warn',

        // Disallow unnecessary escape characters
        // https://eslint.org/docs/latest/rules/no-useless-escape
        'no-useless-escape': 'warn',

        // Disallow renaming import, export, and destructured assignments to the same name
        // https://eslint.org/docs/latest/rules/no-useless-rename
        'no-useless-rename': 'warn',

        // Disallow redundant return statements
        // https://eslint.org/docs/latest/rules/no-useless-return
        'no-useless-return': 'warn',

        // Require let or const instead of var
        // https://eslint.org/docs/latest/rules/no-var
        'no-var': 'warn',

        // Require const declarations for variables that are never reassigned after declared
        // https://eslint.org/docs/latest/rules/prefer-const
        'prefer-const': 'warn',
    }
};
