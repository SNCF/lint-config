module.exports = {
    extends: [
        './rules/best-practices',
        './rules/errors',
        './rules/style'
    ].map(require.resolve),
    env: {
        browser: true,
        node: true,
        amd: false,
        mocha: false,
        jasmine: false
    },
    rules: {
        'no-var': 'off',
        'prefer-object-spread': 'off',
        strict: ['error', 'safe'],
    }
};
